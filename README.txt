README.txt for Admin Toolbar Permissions Module
-----------------------------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Admin Toolbar Permissions Module is a tiny Drupal 8 Module to set the entries in the Drupal 8 Admin Toolbar based on the permissions of a role.

For example: if a role has no access right to manage the Block Layout, this module hides the entry from displaying in the Admin Toolbar Menu.

REQUIREMENTS
------------

No requirements.

RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

This module is currently not listed at [drupal.org](https://www.drupal.org/). I suggest an installation into the `modules/custom` directory using git clone:

```bash
git clone https://gitlab.com/meengit/admintoolbarpermissions.git
```

CONFIGURATION
-------------

No configuration, yust enable to use.

TROUBLESHOOTING
---------------

Nothing known.

FAQ
---

Nothing known.

MAINTAINERS
-----------

* Andreas, <https://gitlab.com/meengit/admintoolbarpermissions>

